#pragma once
#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>

#include "newclass.h" // the class we want to test

class NewClassTesting
	: public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE(NewClassTesting);
	CPPUNIT_TEST(test1);
	CPPUNIT_TEST_SUITE_END();

public:
	NewClassTesting(void);
	~NewClassTesting(void);
	void test1()
	{
		NewClass nc;
		int x = nc.Feature(5);
		//CPPUNIT_ASSERT(x != 5); // error
		CPPUNIT_ASSERT(x == 5); // success
	}
};

